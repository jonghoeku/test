.. mpcv documentation master file, created by
   sphinx-quickstart on Wed Jan 20 13:28:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation
================================

.. toctree::
   :maxdepth: 1

   reference/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

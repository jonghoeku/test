******
Signal
******

.. automodule:: mpcv.signal
.. autosummary::
    :toctree: generated/

    find_two_consecutive_valleys_and_peak_btw_them
    peak_prominences
    savgol_filter

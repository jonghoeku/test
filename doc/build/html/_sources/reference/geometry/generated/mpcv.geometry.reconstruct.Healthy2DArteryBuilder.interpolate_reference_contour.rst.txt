:orphan:

mpcv.geometry.reconstruct.Healthy2DArteryBuilder.interpolate\_reference\_contour
================================================================================

.. currentmodule:: mpcv.geometry.reconstruct

method

.. automethod:: mpcv.geometry.reconstruct.Healthy2DArteryBuilder.interpolate_reference_contour


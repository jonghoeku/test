***********
Graph
***********

.. automodule:: mpcv.graph
.. autosummary::
    :toctree: generated/

    is_graph_connected
    has_graph_selfloops
    has_graph_cycles
    is_bidirected_graph
    find_longest_path
    find_nearest_node_by_coord
    mask_to_sknw_graph
    skeleton_to_sknw_graph
    graph_to_bidirected_graph
    prune_self_loop
    prune_low_weight_leaf_nodes
    prune_pseudo_nodes
    bidirected_graph_to_arborescence
    sparse_graph_to_dense_graph
    max_weighted_shortest_path
    prune_arborescence_and_contour
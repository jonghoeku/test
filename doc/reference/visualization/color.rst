*****
Color
*****

.. automodule:: mpcv.visualization.color
.. autosummary::
    :toctree: generated/

    Color
    color_val

    
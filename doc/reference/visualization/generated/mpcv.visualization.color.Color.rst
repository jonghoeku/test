﻿mpcv.visualization.color.Color
==============================

.. currentmodule:: mpcv.visualization.color

.. autoclass:: Color

   



   

   .. HACK -- the point here is that we don't want this to appear in the output, but the autosummary should still generate the pages.
      .. autosummary::
         :toctree:
      
         Color.black
         Color.blue
         Color.cyan
         Color.green
         Color.magenta
         Color.red
         Color.white
         Color.yellow


*******
Contour
*******

.. automodule:: mpcv.image.contour
.. autosummary::
    :toctree: generated/

    largest_contour

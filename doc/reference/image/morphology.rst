**********
Morphology
**********

.. automodule:: mpcv.image.morphology
.. autosummary::
    :toctree: generated/

    medial_axis
    geodesic_distance_field
    erosion_thickness
    find_centerline_trimming_indices

**
IO
**

.. automodule:: mpcv.image.io
.. autosummary::
    :toctree: generated/

    imread
    imwrite

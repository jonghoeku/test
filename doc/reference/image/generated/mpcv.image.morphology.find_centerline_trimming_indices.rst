﻿

mpcv.image.morphology.find\_centerline\_trimming\_indices
=========================================================

.. currentmodule:: mpcv.image.morphology



.. autofunction:: mpcv.image.morphology.find_centerline_trimming_indices


****
Draw
****

.. automodule:: mpcv.image.draw
.. autosummary::
    :toctree: generated/

    draw_xshape
    polygon_to_mask

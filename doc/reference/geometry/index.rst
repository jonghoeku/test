********
Geometry
********

.. currentmodule:: mpcv.geometry

.. toctree::
    :maxdepth: 2

    angle
    curve
    draw
    linestring
    measure
    point
    polygon
    reconstruct


﻿

mpcv.geometry.linestring.remove\_duplicate\_consecutive\_pts
============================================================

.. currentmodule:: mpcv.geometry.linestring



.. autofunction:: mpcv.geometry.linestring.remove_duplicate_consecutive_pts


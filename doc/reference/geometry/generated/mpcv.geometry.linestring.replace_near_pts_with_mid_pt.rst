﻿

mpcv.geometry.linestring.replace\_near\_pts\_with\_mid\_pt
==========================================================

.. currentmodule:: mpcv.geometry.linestring



.. autofunction:: mpcv.geometry.linestring.replace_near_pts_with_mid_pt


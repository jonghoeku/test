﻿

mpcv.geometry.linestring.nearest\_intersection\_ray\_multiple\_line\_segments
=============================================================================

.. currentmodule:: mpcv.geometry.linestring



.. autofunction:: mpcv.geometry.linestring.nearest_intersection_ray_multiple_line_segments


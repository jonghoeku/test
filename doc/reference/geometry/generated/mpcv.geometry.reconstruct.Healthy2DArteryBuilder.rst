﻿mpcv.geometry.reconstruct.Healthy2DArteryBuilder
================================================

.. currentmodule:: mpcv.geometry.reconstruct

.. autoclass:: Healthy2DArteryBuilder

   

   .. HACK -- the point here is that we don't want this to appear in the output, but the autosummary should still generate the pages.
      .. autosummary::
         :toctree:
      
         Healthy2DArteryBuilder.interpolate_reference_contour
         Healthy2DArteryBuilder.run



   


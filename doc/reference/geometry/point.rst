*****
Point
*****

.. automodule:: mpcv.geometry.point
.. autosummary::
    :toctree: generated/

    is_point_in_polygon
    is_inside_circle
    is_point_on_mask_2d
    intersection_2D_arrays
    is_vector_within_two_vectors_2d
    is_on_segment
    triplet_orientation
    perpendicular_vector_2d
    unit_vector
    closest_target_indices_from_src
    sort_vectors_2d_counterclockwise

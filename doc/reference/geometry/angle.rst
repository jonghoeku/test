*****
Angle
*****

.. automodule:: mpcv.geometry.angle
.. autosummary::
    :toctree: generated/

    signed_angle
    smaller_abs_angle
    unsigned_angle

*******
Polygon
*******

.. automodule:: mpcv.geometry.polygon
.. autosummary::
    :toctree: generated/

    largest_polygon
    create_valid_polygon

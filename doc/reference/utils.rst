*****
Utils
*****

.. automodule:: mpcv.utils
.. autosummary::
    :toctree generated/

    all_equal
    consecutive_groups
    find_double_filtered_idx
    merge_overlapping_intervals
    set_item_in_nested_dict

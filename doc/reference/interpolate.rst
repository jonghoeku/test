***********
Interpolate
***********

.. automodule:: mpcv.interpolate
.. autosummary::
    :toctree: generated/

    interpolate_points_to_n_units
    interpolate_points_to_unit_pixel
    interpolate_points_btw_two_lines
    convert_cubic_bspline_ctrl_pts_to_bezier_ctrl_pts
    cubic_bezier_curve
    quadratic_bezier_curve
    javascript_bezier_curve
    smooth_and_interpolate_2D

*****
Dicom
*****

.. automodule:: mpcv.fileio.dicom
.. autosummary::
    :toctree: generated/

    dcmread
    dcmwrite
    set_pixel_array

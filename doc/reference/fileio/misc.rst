****
Misc
****

.. automodule:: mpcv.fileio.misc
.. autosummary::
    :toctree: generated/

    load
    dump
    numpy_type_to_builtin
    NumpyEncoder
    builtin_type_to_numpy
    encode_image_to_base64
    decode_base64_to_image

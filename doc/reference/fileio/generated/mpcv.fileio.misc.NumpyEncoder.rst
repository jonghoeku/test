﻿mpcv.fileio.misc.NumpyEncoder
=============================

.. currentmodule:: mpcv.fileio.misc

.. autoclass:: NumpyEncoder

   

   .. HACK -- the point here is that we don't want this to appear in the output, but the autosummary should still generate the pages.
      .. autosummary::
         :toctree:
      
         NumpyEncoder.default
         NumpyEncoder.encode
         NumpyEncoder.iterencode



   

   .. HACK -- the point here is that we don't want this to appear in the output, but the autosummary should still generate the pages.
      .. autosummary::
         :toctree:
      
         NumpyEncoder.item_separator
         NumpyEncoder.key_separator


:orphan:

mpcv.fileio.misc.NumpyEncoder.item\_separator
=============================================

.. currentmodule:: mpcv.fileio.misc

attribute

.. autoattribute:: mpcv.fileio.misc.NumpyEncoder.item_separator


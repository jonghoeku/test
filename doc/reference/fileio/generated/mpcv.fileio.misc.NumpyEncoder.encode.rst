:orphan:

mpcv.fileio.misc.NumpyEncoder.encode
====================================

.. currentmodule:: mpcv.fileio.misc

method

.. automethod:: mpcv.fileio.misc.NumpyEncoder.encode


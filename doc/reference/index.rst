.. _reference:

Reference
*********

   :Release: |release|
   :Date: |today|

.. toctree::
   :maxdepth: 4

   fileio/index
   geometry/index
   image/index
   signal
   interpolate
   graph
   visualization/index
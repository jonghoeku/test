﻿

mpcv.interpolate.convert\_cubic\_bspline\_ctrl\_pts\_to\_bezier\_ctrl\_pts
==========================================================================

.. currentmodule:: mpcv.interpolate



.. autofunction:: mpcv.interpolate.convert_cubic_bspline_ctrl_pts_to_bezier_ctrl_pts


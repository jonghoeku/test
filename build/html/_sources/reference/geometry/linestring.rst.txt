**********
Linestring
**********

.. automodule:: mpcv.geometry.linestring
.. autosummary::
    :toctree: generated/

    intersection_two_line_segments
    nearest_intersection_ray_multiple_line_segments
    remove_duplicate_consecutive_pts
    replace_near_pts_with_mid_pt
    ordered_contour_indices

